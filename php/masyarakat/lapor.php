<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIPKIF</title>

    <!-- Bootstrap core CSS -->
    <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="../../vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/creative.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../css/laporan.css">
    <!-- <link rel="stylesheet" href="css/style.css"> -->
    <style>
            .btn-primary {
              background-color: #00004d;
              border-color: #F05F40;
              color: #ffff00;
              margin-left: 460px;
            }

            .btn-primary:hover, .btn-primary:focus, .btn-primary:active {
              color: #00004d;
              background-color: #ffff00 !important;
            }

    </style>

  </head>
<body id="page-top">

    <!-- Navigation -->
    <nav style="background-color: #00004d" class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="indexMasyarakat.php">Tayo</a>
      </div>
    </nav>

<div class="container2">
		<h2>Laporan</h2>
		<p>Laporkan kerusakan yang ada melalui form dibawah ini :</p>
         <form action="" method="post">

              <div class="customForm">
                <table>                    
                    <tr>
                        <td><label for="infrastruktur">Nama infrastruktur</label></td>
                        <td>:</td>
                        <td><input class="form-control" type="text" placeholder="Masukkan Nama Infrastruktur" id="infrastruktur"></td>
                    </tr>
                    <tr>
                        <td><label for="infrastruktur">Jenis infrastruktur</label></td>
                        <td>:</td>
                        <td><input class="form-control" type="text" placeholder="Masukkan Jenis Infrastruktur" id="infrastruktur"></td>
                    </tr>
                    <tr>
                        <td><label for="exampleFormControlFile1">Masukkan photo</label></td>
                        <td>:</td>
                        <td><input type="file" class="form-control-file" id="exampleFormControlFile1"></td>
                    </tr>
                    <tr>
                        <td><label for="infrastruktur">Lokasi Kerusakan</label></td>
                        <td>:</td>
                        <td><input class="form-control" type="text" placeholder="Masukkan Lokasi Kerusakan" id="infrastruktur"></td>
                    </tr> 
                    <div class="spasi">
                    <tr>    
                        <td><label for="exampleFormControlSelect1">Tingkat Kerusakan</label></td>
                        <td>:</td>
                        <td><select class="form-control" id="exampleFormControlSelect1">
                          <option>Ringan</option>
                          <option>Sedang</option>
                          <option>Berat</option>
                        </select></td>
                    </tr>

                    <div class="spasi">
                    <tr> 
                        <td><label for="exampleFormControlTextarea1">Keterangan Tambahan</label></td>
                        <td>:</td>
                        <td><textarea class="form-control" id="exampleFormControlTextarea1" rows="5 cols="4"></textarea>
                        </table></td>
                    </tr> 
                    <tr>
                        <td colspan="3">
                            <a class="btn btn-primary js-scroll-trigger" href="#about">Kirim</a>
                            
                        </td>
                    </tr>
              </div>
         </form>
    </div>


    <footer>
        <div class="footer">
            <p >&copy Tayo 2018</p>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../../vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="../../vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../../js/creative.min.js"></script>

  </body>

</html>

