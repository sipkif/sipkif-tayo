<?php 
require '../functions.php';

session_start();

if(!isset($_SESSION["login"])){
  header("location:../../login/loginMasyarakat.php");
  exit;
}

 ?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIPKIF</title>

    <!-- Bootstrap core CSS -->
    <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="../../vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/creative.css" rel="stylesheet">
    <style>
      .footer{
        background-color: #ffff00;
        color: #00004d;
        text-align: center;
        height: 50px;
        padding-top: 11px;

      }
      h3{
        color: #00004d;
        text-decoration: none;
        transition: .6s;
      }
      a h3:hover {
      color: #ffff00;
      text-decoration: none;
      font-size: 25px;
      text-shadow: 0 0 3px #00004d;
      }
    </style>

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav style="background-color: #00004d" class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Tayo</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span style="background-color: #e6e6e6" class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Tentang</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Layanan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Kontak</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../../login/logoutMasyarakat.php">Keluar</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>SIPKIF</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5">Sistem Informasi Pendataan Kerusakan Infrastruktur dan Fasilitas Publik</p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Ketahui Lebih Jauh</a>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">Apa yang bisa anda lakukan dengan SIPKIF ?</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">Dengan SIPKIF anda dapat membantu mempermudah kinerja BPBD
              dalam mengelola data kerusakan infrastruktur dan fasilitas publik yang ada pada suatu wilayah
              dengan meminta peran langsung dari masyarakat yaitu dengan cara melaporkan suatu kerusakan
              infrastruktur dan fasilitas publik melalui SIPKIF ini.</p>
            <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">AYO MULAI!</a>
          </div>
        </div>
      </div>
    </section>

    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Layanan</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <a href="lapor.php">
              <img src="../../img/icon/lapor.png">
              <h3 class="mb-3">Lapor</h3>
              </a>
              <p class="text-muted mb-0">Lapor kerusakan infrastruktur</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <a href="cekLaporan.php">
              <img src="../../img/icon/cekLaporan.png">
              <h3 class="mb-3">Cek status Laporan</h3>
              </a>
              <p class="text-muted mb-0">Kamu bisa melihat status laporan</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <a href="profil.php">
              <img src="../../img/icon/editProfile.png">
              <h3 class="mb-3">Ubah profil</h3>
              </a>
              <p class="text-muted mb-0">Ubah Profil Anda</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <a href="">
              <img src="../../img/icon/bpbd2.png">
              <h3 class="mb-3">Informasi</h3>
              </a>
              <p class="text-muted mb-0">Cari informasi tentang BPBD</p>
            </div>
          </div>
        </div>
      </div>

    </section>


    <section class="bg-dark text-white" id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Kontak</h2>
            <hr class="my-4">
            <p class="mb-5">Anda memerlukan bantuan ? Hubungi kami atau kirim email kepada kami dan kami akan menghubungi Anda sesegera mungkin</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 ml-auto text-center">
            <i class="fas fa-phone fa-3x mb-3 sr-contact-1"></i>
            <p>123-456-6789</p>
          </div>
          <div class="col-lg-4 mr-auto text-center">
            <i class="fas fa-envelope fa-3x mb-3 sr-contact-2"></i>
            <p>
              <a href="mailto:your-email@your-domain.com">feedback@startbootstrap.com</a>
            </p>
          </div>
        </div>
      </div>
    </section>

    <footer>
        <div class="footer">
            <p >&copy Tayo 2018</p>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../../vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="../../vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../../js/creative.min.js"></script>

  </body>

</html>
