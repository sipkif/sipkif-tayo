<?php 

// memanggil file functions
// include atau require
require '../functions.php';

 ?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIPKIF</title>

    <!-- Bootstrap core CSS -->
    <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="../../vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/creative.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="css/style.css"> -->
    <style>
            .btn-primary {
              background-color: #00004d;
              border-color: #F05F40;
              color: #ffff00;
              margin-top: 20px;
            }

            .btn-primary:hover, .btn-primary:focus, .btn-primary:active {
              color: #00004d;
              background-color: #ffff00 !important;
            }
            .containerProfile{
            	padding-top: 100px;
  				padding-left: 160px;

            }
            body{
			  background-image: url(../../img/houndstooth-pattern.png);
			}
			.footer{
			        background-color: #ffff00;
			        color: #00004d;
			        text-align: center;
			        height: 50px;
			        padding-top: 11px;
			        margin-top: 104px;

			}
            table{
            	margin-top: 50px;
            	margin-left: -140px;
            }

    </style>

  </head>
<body id="page-top">

    <!-- Navigation -->
    <nav style="background-color: #00004d" class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="indexMasyarakat.php">Tayo</a>
      </div>
    </nav>
		
	
</div>
</nav>
<div class="containerProfile">
		<h2>Profile</h2>
	
		
	</div>
	<center>
	<img src="../../img/user.png" width="200">	
	<table >
        <center>
            <tr>
            	<td>Nama</td>
            	<td>:</td>
                <td>nama</td>                
            </tr>
            <tr>
            	<td>Username</td>
            	<td>:</td>
                <td>username</td>
            </tr>
            <tr>
            	<td>Email</td>
            	<td>:</td>
                <td>email</td>
            </tr>
            <tr>
            	<td>Alamat</td>
            	<td>:</td>
                <td>alamat</td>
            </tr>
            <tr>
            	<td>Telpon</td>
            	<td>:</td>
                <td>telpon</td>
            </tr>
            <tr>
            	<td>Pekerjaan</td>
            	<td>:</td>
                <td>pekerjaan</td>
            </tr>
              </center>
            
        </table>
        <button class="btn btn-primary js-scroll-trigger" name="submit" onclick="submit" value="submit">Edit</button>
    </center>
</div>


<footer>
        <div class="footer">
            <p >&copy Tayo 2018</p>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../../vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="../../vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../../js/creative.min.js"></script>

  </body>

</html>
