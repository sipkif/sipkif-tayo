<?php 

session_start();
require '../php/functions.php';

// cek cookie
if(isset($_COOKIE['id']) && isset($_COOKIE['key'])){
	$id = $_COOKIE['id'];
	$key = $_COOKIE['key'];
	
	// ambil username dan id
	$result = mysqli_query($db, "SELECT username FROM user WHERE id = $id");
	$row = mysqli_fetch_assoc($result);

	// cek cookie dan username
	if($key === hash('sha256',$row['username'])){
		$_SESSION['login']=true;
	}
}


if(isset($_SESSION["login"])){
	header("Location:../php/masyarakat/indexMasyarakat.php");
	exit;
}



if(isset($_POST["login"])){
	$username = $_POST["username"];
	$password1 = $_POST["password1"];
	$result = mysqli_query($db, "SELECT * FROM user WHERE username = '$username'");

	// cek username
	if(mysqli_num_rows($result) === 1){
		// cek password
		$row = mysqli_fetch_assoc($result);
		if(password_verify($password1, $row["password1"])){
			// cek sesion
			$_SESSION["login"] = true;

			// cek remember me
			if(isset($_POST['remember'])){
				// buat cookie

				setcookie('id',$row['id'],time()+60);
				setcookie('key',hash('sha256',$row['username']),time()+60);
			}

			header("location:../php/masyarakat/indexMasyarakat.php");
			exit;
		}

	}
	$error = true;
}


 ?>


<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>Login</title>	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script type="text/javascript" src="../js/custom.js"></script>
	<!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="../vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/creative.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="css/style.css"> -->
    <script type="text/javascript" src="../js/custom.js"></script>
	<style>
		.btn-primary {
              background-color: #00004d;
              border-color: #F05F40;
              color: #ffff00;
              margin-left: 160px;
            }

            .btn-primary:hover, .btn-primary:focus, .btn-primary:active {
              color: #00004d;
              background-color: #ffff00 !important;
            }
         .footer{
		        background-color: #ffff00;
		        color: #00004d;
		        text-align: center;
		        height: 50px;
		        padding-top: 11px;
		        margin-top: -50px;

		}
		.salah{
			color: red; 
			font-style: italic;
			position: absolute;
			margin-top: 40px;
			margin-left: 80px
		}
	</style>
</head>
<body id="page-top">
	<!-- header -->
		<!-- Navigation -->
    <nav style="background-color: #00004d" class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../index.php">Tayo</a>
      </div>
    </nav>

	<!-- body -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-25 p-b-20">
				<form class="login100-form validate-form" action="" method="post">
					<span class="login100-form-avatar">
						<img src="../img/icon/masyarakat.png">
					</span>
						
					<?php if(isset($error)): ?>
						<p class="salah">username atau password salah</p>
	  				 <?php endif; ?>

					<div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate = "Enter username">
						<input class="input100" type="text" name="username" required autofocus>
						<span class="focus-input100" data-placeholder="Nama Pengguna"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-15" data-validate="Enter password">
						<input class="input100" type="password" name="password1" id="password1" required>
						<span class="focus-input100" data-placeholder="Kata Sandi"></span>
					</div>

					<div class="txt2">
       					 <label>
       					   <input type="checkbox" onclick="myFunction()"> Lihat Kata Sandi <br>
         					 <input name="remember" type="checkbox" value="remember-me"> Ingatkan saya
       					 </label>
      					</div>
					<div>
						<button class="btn btn-primary js-scroll-trigger" name="login">Masuk</button>
					</div><br>
							<span style="margin-left: 75px" class="txt1">
								Belum mempunyai akun ?
							</span>

							<a href="registrasiMasyarakat.php" class="txt2">
								Daftar
							</a>
				</form>
			</div>
		</div>
	</div>
	
	<!-- footer -->
	<footer>
        <div class="footer">
            <p >&copy Tayo 2018</p>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="../vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../js/creative.min.js"></script>


	<div id="dropDownSelect1"></div>
	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<script src="js/main.js"></script>


</body>
</html>