<?php 
	require '../php/functions.php';
	if(isset($_POST["register"])){
		if(registrasi($_POST)>0){
			echo "<script>
				alert('Akun berhasil di tambahkan');
				document.location.href = 'loginMasyarakat.php';
			</script";
		}else{
			echo mysqli_error($db);
		}
	}

 ?>

<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>Pendaftaran</title>	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script type="text/javascript" src="../js/custom.js"></script>
	<!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="../vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/creative.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="css/style.css"> -->
	<style>
		.btn-primary {
              background-color: #00004d;
              border-color: #F05F40;
              color: #ffff00;
              margin-left: 160px;
            }

            .btn-primary:hover, .btn-primary:focus, .btn-primary:active {
              color: #00004d;
              background-color: #ffff00 !important;
            }
         .footer{
		        background-color: #ffff00;
		        color: #00004d;
		        text-align: center;
		        height: 50px;
		        padding-top: 11px;
		        margin-top: 20px;px;

		}
	</style>
</head>
<body id="page-top">
	<!-- header -->
		<!-- Navigation -->
    <nav style="background-color: #00004d" class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../index.php">Tayo</a>
      </div>
    </nav>

	<!-- body -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-85 p-b-20">
				<span class="login100-form-title p-b-70">
						Forum Pendaftaran
					</span>
				<form class="login100-form validate-form" action="" method="post">
					<input type="hidden" name="gambar" value="user.png">

					<div class="wrap-input100 validate-input m-b-35" data-validate = "Enter username">
						<input class="input100" type="text" name="nama" required>
						<span class="focus-input100" data-placeholder="Nama Lengkap"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-35" data-validate = "Enter username">
						<input class="input100" type="text" name="username" required>
						<span class="focus-input100" data-placeholder="Nama Pengguna"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-35" data-validate = "Enter username">
						<input class="input100" type="email" name="email">
						<span class="focus-input100" data-placeholder="Masukkan Email"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-50" data-validate="Enter password">
						<input class="input100" type="password" name="password1" id="password1" required>
						<span class="focus-input100" data-placeholder="Kata Sandi"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-50" data-validate="Enter password">
						<input class="input100" type="password" name="password2" id="password2" required>
						<span class="focus-input100" data-placeholder="Konfirmasi Kata Sandi"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-35" data-validate = "Enter username">
						<textarea class="input100" type="text" name="alamat" required></textarea>
						<span class="focus-input100" data-placeholder="Masukkan Alamat"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-35" data-validate = "Enter username">
						<input class="input100" type="text" name="telpon" required>
						<span class="focus-input100" data-placeholder="Masukkan Nomor Telpon"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-35" data-validate = "Enter username">
						<input class="input100" type="text" name="pekerjaan" required>
						<span class="focus-input100" data-placeholder="Masukkan Pekerjaan"></span>
					</div>
					<div>
						<button class="btn btn-primary js-scroll-trigger" name="register">Daftar</button>
					</div><br>
					<span style="margin-left: 40px" class="txt1">
								Sudah mempunyai akun ?
							</span>

							<a href="loginMasyarakat.php" class="txt2">
								Masuk sekarang
							</a>
				</form>
			</div>
		</div>
	</div>
	
	<!-- footer -->
	<footer>
        <div class="footer">
            <p >&copy Tayo 2018</p>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="../vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../js/creative.min.js"></script>


	<div id="dropDownSelect1"></div>
	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<script src="js/main.js"></script>


</body>
</html>